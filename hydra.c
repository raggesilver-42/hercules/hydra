/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hydra.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pqueiroz <pqueiroz@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 15:29:27 by pqueiroz          #+#    #+#             */
/*   Updated: 2019/03/08 22:54:21 by pqueiroz         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/socket.h>

#define PORT 5555
#define ERR_INVALID_ARGS -1
#define ERR_EADDR_IN_USE -2
#define ERR_INVALID_SOCDESC -3
#define ERR_FORK_FAILED -4
#define ERR_SETSID_FAILED -5

typedef struct sockaddr_in	t_addr;
const char					g_res[9] = "pong pong";
int							g_socfd;

void	sig_handler(int sig)
{
	(void)sig;
	shutdown(g_socfd, SHUT_RDWR);
	close(g_socfd);
	printf("Terminating\n");
	exit(0);
}

/*
** 1st exit(0): quit the main thread
** setsid(): become the session leader
** 2nd exit(0): quit the non-leader session
** closes(0-2): close stdin, stdout, stderr
** opens(0-2): open stdin, stderr, stdout to /dev/null (supress output)
*/

void	daemonize(void)
{
	pid_t	pid;

	if ((pid = fork()) < 0)
		exit(ERR_FORK_FAILED);
	else if (pid > 0)
		exit(0);
	if (setsid() < 0)
		exit(ERR_SETSID_FAILED);
	if ((pid = fork()) < 0)
		exit(ERR_FORK_FAILED);
	else if (pid > 0)
		exit(0);
	close(0);
	close(1);
	close(2);
	open("/dev/null", O_RDONLY);
	open("/dev/null", O_RDWR);
	open("/dev/null", O_RDWR);
}

int		serve_loop(int fd)
{
	t_addr		client;
	int			client_fd;
	socklen_t	client_size;
	char		buff[5];

	buff[4] = 0;
	printf("Server started!\n");
	while (1)
	{
		client_size = sizeof(client);
		if ((client_fd = accept(fd, (struct sockaddr *)&client, &client_size))
			< 0)
			continue ;
		read(client_fd, buff, 4);
		printf("Client '%s'\n", buff);
		if (strcmp(buff, "ping") == 0)
		{
			write(client_fd, g_res, 9);
		}
		shutdown(client_fd, SHUT_RDWR);
		close(client_fd);
	}
	return (0);
}

int		serve(void)
{
	t_addr	server;
	int		op;

	op = 1;
	g_socfd = socket(AF_INET, SOCK_STREAM, 0);
	if (g_socfd < 0)
		return (ERR_INVALID_SOCDESC);
	if (setsockopt(g_socfd, SOL_SOCKET, SO_REUSEADDR, (void *)&op,
		sizeof(op)) != 0)
		perror("Could not set socket option!\n");
	bzero((char *)&server, sizeof(server));
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);
	server.sin_family = AF_INET;
	if (bind(g_socfd, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		exit(ERR_EADDR_IN_USE);
	}
	if (listen(g_socfd, 8) < 0)
		return (ERR_INVALID_SOCDESC);
	return (serve_loop(g_socfd));
}

int		main(int argc, char **argv)
{
	if (argc == 2 && strcmp("-D", argv[1]) == 0)
	{
		daemonize();
		return (serve());
	}
	else if (argc == 1)
	{
		signal(SIGINT, &sig_handler);
		return (serve());
	}
	return (ERR_INVALID_ARGS);
}
