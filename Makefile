
NAME=hydra

FLAGS=-Wall -Werror -Wextra

FILES=hydra.c

OBJECTS=$(FILES:.c=.o)

INCDIR=.

all: $(NAME)

%.o: %.c
	gcc $(FLAGS) -I$(INCDIR) -o $@ -c $<

$(NAME): $(OBJECTS)
	gcc $(FLAGS) -o $@ $^

check:
	norminette $(FILES)

clean:
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)

run:
	./$(NAME)

re: fclean all

test: re run

.PHONY: all re fclean clean test
